import torch
from torch import nn
from torchvision import datasets
from torchvision.transforms import ToTensor, Lambda, Compose
from torch.utils.data import DataLoader
import matplotlib.pyplot as plt

from ClassifierNet import NeuralNet
from GeneratorNet import GeneratorNet

labels_map = {
    0: "T-Shirt",
    1: "Trouser",
    2: "Pullover",
    3: "Dress",
    4: "Coat",
    5: "Sandal",
    6: "Shirt",
    7: "Sneaker",
    8: "Bag",
    9: "Ankle Boot",
}

def train_loop(dataloader, model, loss_fn, optimizer):
    size = len(dataloader.dataset)
    for batch, (X, y) in enumerate(dataloader):
        # Compute prediction and loss
        pred = model(X)
        loss = loss_fn(pred, y)

        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch % 100 == 0:
            loss, current = loss.item(), batch * len(X)
            print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")


def test_loop(dataloader, model, loss_fn):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    test_loss, correct = 0, 0

    with torch.no_grad():
        for Y, x in dataloader:
            pred = model(Y)
            test_loss += loss_fn(pred, x).item()
            correct += (pred.argmax(1) == x).type(torch.float).sum().item()

    test_loss /= num_batches
    correct /= size
    print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")

#Define model

classifier = torch.load("models\\initial.pt")
model = GeneratorNet()
model.eval()

print(model)

#Download training data

ds = datasets.FashionMNIST(root="data", train=True, download=True, transform=ToTensor(),
                           target_transform=Lambda(
                               lambda y: torch.zeros(10, dtype=torch.float).scatter_(0, torch.tensor(y), 1)))

training_data = datasets.FashionMNIST(
    root="data",
    train=True,
    download=True,
    transform=ToTensor()
    #transform=Compose([ToTensor(), Lambda(lambda x: torch.flatten(x))])
    #target_transform=Lambda(
     #   lambda y: torch.zeros(10, dtype=torch.float).scatter_(0, torch.tensor(y), 1))
)

test_data = datasets.FashionMNIST(
    root="data",
    train=False,
    download=True,
    transform=ToTensor()
    #transform=Compose([ToTensor(), Lambda(lambda x: torch.flatten(x))])
    #target_transform=Lambda(
    #    lambda y: torch.zeros(10, dtype=torch.float).scatter_(0, torch.tensor(y), 1))
)

train_dataloader = DataLoader(training_data, batch_size=32)
test_dataloader = DataLoader(test_data, batch_size=32)

# Learning

optimizer = torch.optim.SGD(model.parameters(), lr=1e-2)
lossFn = nn.BCEWithLogitsLoss()

# Forward pass

image, label = ds[0]
prediction = model(image)
loss = lossFn(prediction, image)
loss.backward()
optimizer.step()

for t in range(10):
    print("Epoch")
    train_loop(train_dataloader, model, lossFn, optimizer)
    test_loop(test_dataloader, model, lossFn)

    # Visualize Generation

    figure = plt.figure(figsize=(8, 8))

    sample_idx = torch.randint(len(ds.train_data), size=(1,)).item()
    image, label = ds[sample_idx];
    # plt.imshow(label.squeeze(), cmap="gray")
    plt.imshow(model(label), cmap="gray")
    plt.show()

print("Training over")

newPrediction = model(input)

print(prediction)
print(newPrediction)

model.eval()
torch.save(model.state_dict(), "models\\generator.pt")

# prediction = model(data) # forward pass

# loss = (prediction - labels).sum()
# loss.backward() # backward pass

# optim = torch.optim.SGD(model.parameters(), lr=1e-2, momentum=0.9)

# optim.step() #gradient descent
