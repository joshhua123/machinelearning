import torch
from torch import nn


class GeneratorNet(nn.Module):
    def __init__(self):
        super(GeneratorNet, self).__init__()
        self.flatten = nn.Flatten()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(10, 512),
            nn.ReLU(),
            nn.Linear(512, 512),
            nn.ReLU(),
            nn.Linear(512, 28*28),
            nn.ReLU(),
        )


    def forward(self, x):
        #flat = torch.flatten(x, start_dim=1)
        #flat = self.flatten(x)
        #logits = self.linear_relu_stack(flat)
        return x